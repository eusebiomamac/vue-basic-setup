#!/bin/bash
echo "Copying used icons..."

rm ./src/used-icons/index.js
touch ./src/used-icons/index.js

# Manually add icons here
echo "arrow-left" >> icons-used.txt
echo "dots-horizontal" >> icons-used.txt
echo "pencil" >> icons-used.txt


# Loop through found used icons and copy them to ./src/used-icons while adding requires to ./src/used-icons/index.js for import.

cat icons-used.txt | while read line
do
  # do something with $line here
  echo "Adding used icon: $line"
  cp ./src/compiled-icons/${line}.js ./src/used-icons
  echo "require('./${line}')" >> ./src/used-icons/index.js
done
