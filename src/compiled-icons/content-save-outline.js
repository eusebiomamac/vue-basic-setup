/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'content-save-outline': {
    width: 24,
    height: 24,
    viewBox: '0 0 24.00 24.00',
    data: '<path pid="0" d="M15 5v4H5v10h3a5 5 0 1 1 8 0h3V7.828L16.172 5H15zM5 7h8V5H5v2zm12-4l4 4v12c0 1.1-.9 2-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h12zm-5 10a3 3 0 0 0 0 6 3 3 0 0 0 0-6z"/>'
  }
})
